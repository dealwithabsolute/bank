CREATE TABLE IF NOT EXISTS bank.auth
(
    id          BIGINT PRIMARY KEY NOT NULL,
    user_id      BIGINT,
    blocked     BOOLEAN  default false,
    token       VARCHAR(256),
    fail_counter SMALLINT default 0,
    pin         SMALLINT,
    auth_type    VARCHAR(256)
);

CREATE SEQUENCE bank.auth_seq
    start with 1
    increment by 1;

alter table bank.auth
    owner to postgres;

