CREATE SCHEMA IF NOT EXISTS bank AUTHORIZATION postgres;

CREATE TABLE IF NOT EXISTS bank.user
(
    id      BIGINT PRIMARY KEY NOT NULL,
    balance NUMERIC
);

CREATE SEQUENCE user_seq
    start with 1
    increment by 1;

alter table bank.user
    owner to postgres;

