package com.emulator.bank.repository;

import com.emulator.bank.models.Auth;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthRepository extends CrudRepository<Auth, Long> {

    List<Auth> findAuthByUserId(Long userId);

}
