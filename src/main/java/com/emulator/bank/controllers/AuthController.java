package com.emulator.bank.controllers;

import com.emulator.bank.service.AuthService;
import com.emulator.bank.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class AuthController {

    private final UserService userService;
    private final AuthService authService;

    @Autowired
    public AuthController(UserService userService, AuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/auth/login")
    public ResponseEntity<String> login(@RequestParam(value = "user") Long userId,
                                        @RequestParam(value = "pin") Integer pin) {
        try {
            if (userService.findUser(userId).isEmpty()) {
                return ResponseEntity.notFound().build();
            }

            if (authService.isBlocked(userId)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("user is blocked");
            }

            if (authService.isPinOk(userId, pin)) {
                return ResponseEntity.ok(authService.getToken(userId));
            } else {
                authService.iterateError(userId);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("pin is not correct");
            }

        } catch (Exception e) {
            log.error("Error while checking pin user:{}, pin:{}", userId, pin, e);
            return ResponseEntity.internalServerError().build();
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/auth/changePin")
    public ResponseEntity<String> changePin(@RequestParam(value = "user") Long userId,
                                         @RequestParam(value = "token") String token,
                                         @RequestParam(value = "pin") Integer pin) {
        try {

            if (userService.findUser(userId).isEmpty()) {
                return ResponseEntity.notFound().build();
            }

            if (authService.isBlocked(userId)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("user is blocked");
            }

            if (!authService.isLoggedIn(userId, token)) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("not logged in");
            }

            if (authService.updatePin(userId, pin)) {
                return ResponseEntity.ok(null);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/auth/updateAuthType")
    public ResponseEntity<String> updateAuthType(@RequestParam(value = "user") Long userId,
                                                 @RequestParam(value = "authType") String authType,
                                                 @RequestParam(value = "token") String token) {
        try {
            if (userService.findUser(userId).isEmpty()) {
                return ResponseEntity.notFound().build();
            }

            if (authService.isBlocked(userId)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("user is blocked");
            }

            if (!authService.isLoggedIn(userId, token)) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("not logged in");
            }

            return ResponseEntity.status(HttpStatus.OK).body(String.valueOf(authService.updateAuthType(userId, authType)));

        } catch (Exception e) {
            log.error("Error while updating auth type pin user:{}, token:{}", userId, token, e);
            return ResponseEntity.internalServerError().build();
        }
    }
}
