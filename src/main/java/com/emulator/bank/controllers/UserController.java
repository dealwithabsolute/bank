package com.emulator.bank.controllers;


import com.emulator.bank.service.AuthService;
import com.emulator.bank.service.OperationService;
import com.emulator.bank.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@Slf4j
@RestController
public class UserController {

    private final UserService userService;
    private final OperationService operationService;
    private final AuthService authService;

    @Autowired
    public UserController(UserService userService,
                          OperationService operationService,
                          AuthService authService) {
        this.userService = userService;
        this.operationService = operationService;
        this.authService = authService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/create")
    public String createUser() {
        try {
            final Long id = userService.create().getId();
            authService.create(id);
            return id.toString();
        } catch (Exception e) {
            log.error("Error while creating user", e);
            return e.getMessage();
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "user/showAll")
    public ResponseEntity<String> showAll() {
        try {
            return ResponseEntity.ok(userService.getAllUsers().toString());
        } catch (Exception e) {
            log.error("Error while getting all user", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/delete")
    public ResponseEntity<String> deleteUser(@RequestParam(value = "id") Long userId) {
        try {
            userService.deleteUser(userId);
            authService.delete(userId);
            return ResponseEntity.ok(null);
        } catch (Exception e) {
            log.error("Error while deleting user", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/giveMoney")
    public ResponseEntity<String> giveMoney(@RequestParam(value = "user") Long userId,
                                            @RequestParam(value = "token") String token,
                                            @RequestParam(value = "amount") Double amount) {
        try {
            if (authService.isLoggedIn(userId, token)) {
                if (operationService.giveMoney(userId, BigDecimal.valueOf(amount))) {
                    return ResponseEntity.ok(null);
                } else {
                    return ResponseEntity.notFound().build();
                }
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("you are not logged in");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/takeMoney")
    public ResponseEntity<String> takeMoney(@RequestParam(value = "user") Long userId,
                                            @RequestParam(value = "token") String token,
                                            @RequestParam(value = "amount") Double amount) {
        try {
            if (authService.isLoggedIn(userId, token)) {
                if (operationService.takeMoney(userId, BigDecimal.valueOf(amount))) {
                    return ResponseEntity.ok(null);
                } else {
                    return ResponseEntity.notFound().build();
                }
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("you are not logged in");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/giveBalance")
    public ResponseEntity<String> showBalance(@RequestParam(value = "user") Long userId, @RequestParam(value = "token") String token) {
        try {
            if (authService.isLoggedIn(userId, token)) {
                return ResponseEntity.ok().body(operationService.showBalance(userId).toString());
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("you are not logged in");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "user/transfer")
    public ResponseEntity<String> transfer(@RequestParam(value = "from") Long fromId,
                                           @RequestParam(value = "to") Long toId,
                                           @RequestParam(value = "amount") BigDecimal amount,
                                           @RequestParam(value = "token") String token) {
        try {
            if (authService.isLoggedIn(fromId, token)) {
                return ResponseEntity.ok(String.valueOf(operationService.transfer(fromId, toId, amount)));
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("you are not logged in");
            }
        } catch (Exception e) {
            log.error("Error while transfer money from user to user", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
