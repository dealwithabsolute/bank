package com.emulator.bank.service;

import com.emulator.bank.models.User;

import java.util.Optional;

public interface UserService {

    User create();

    User save(User user);

    Optional<User> findUser(Long id);

    Iterable<User> getAllUsers();

    void deleteUser(Long id);
}
