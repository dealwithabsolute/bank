package com.emulator.bank.service;

import java.math.BigDecimal;

public interface OperationService {

    boolean transfer(Long fromId, Long toId, BigDecimal amount);

    boolean giveMoney(Long id, BigDecimal amount);

    boolean takeMoney(Long id, BigDecimal amount);

    BigDecimal showBalance(Long id);
}
