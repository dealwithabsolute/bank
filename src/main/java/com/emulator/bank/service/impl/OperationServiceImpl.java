package com.emulator.bank.service.impl;

import com.emulator.bank.models.User;
import com.emulator.bank.service.OperationService;
import com.emulator.bank.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Slf4j
@Service
public class OperationServiceImpl implements OperationService {

    private final UserService userService;

    @Autowired
    public OperationServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean transfer(Long fromId, Long toId, BigDecimal amount) {
        try {
            Optional<User> fromUserOpt = userService.findUser(fromId);
            Optional<User> toUserOpt = userService.findUser(toId);
            if (toUserOpt.isPresent() && fromUserOpt.isPresent()) {
                User fromUser = fromUserOpt.get();
                User toUser = toUserOpt.get();
                fromUser.minusBalance(amount);
                toUser.plusBalance(amount);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean giveMoney(Long id, BigDecimal amount) {
        Optional<User> userOpt = userService.findUser(id);
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            user.plusBalance(amount);
            userService.save(user);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean takeMoney(Long id, BigDecimal amount) {
        Optional<User> userOpt = userService.findUser(id);
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            user.minusBalance(amount);
            userService.save(user);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public BigDecimal showBalance(Long id) {
        Optional<User> userOpt = userService.findUser(id);
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            return user.getBalance();
        } else {
            return BigDecimal.ZERO;
        }
    }
}
