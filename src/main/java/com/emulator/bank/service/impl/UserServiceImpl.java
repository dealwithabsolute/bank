package com.emulator.bank.service.impl;

import com.emulator.bank.models.Auth;
import com.emulator.bank.models.User;
import com.emulator.bank.repository.AuthRepository;
import com.emulator.bank.repository.UserRepository;
import com.emulator.bank.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AuthRepository authRepository;
    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           AuthRepository authRepository) {
        this.userRepository = userRepository;
        this.authRepository = authRepository;
    }

    @Override
    public User create() {
        User user = new User();
        userRepository.save(user);
        return user;
    }

    @Override
    public User save(User user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public Optional<User> findUser(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}
