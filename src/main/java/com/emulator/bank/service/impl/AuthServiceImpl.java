package com.emulator.bank.service.impl;

import com.emulator.bank.models.Auth;
import com.emulator.bank.repository.AuthRepository;
import com.emulator.bank.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class AuthServiceImpl implements AuthService {

    private final AuthRepository authRepository;

    @Autowired
    public AuthServiceImpl(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    @Override
    public String getToken(Long userId) {
        final List<Auth> content = authRepository.findAuthByUserId(userId);
        if (!CollectionUtils.isEmpty(content)) {
            return content.get(0).getToken();
        } else {
            return Strings.EMPTY;
        }
    }

    @Override
    public boolean isPinOk(Long userId, Integer pin) {
        final List<Auth> content = authRepository.findAuthByUserId(userId);
        if (!CollectionUtils.isEmpty(content)) {
            return content.get(0).getPin().equals(pin);
        } else {
            return false;
        }
    }

    @Override
    public boolean isBlocked(Long userId) {
        final List<Auth> content = authRepository.findAuthByUserId(userId);
        if (!CollectionUtils.isEmpty(content)) {
            return content.get(0).isBlocked();
        } else {
            return false;
        }
    }

    @Override
    public void iterateError(Long userId) {
        final List<Auth> content = authRepository.findAuthByUserId(userId);
        if (!CollectionUtils.isEmpty(content)) {
            Auth auth = content.get(0);
            auth.plusFailCounter();
            if (auth.getFailCounter() >= 3) {
                auth.setBlocked(true);
            }
            authRepository.save(auth);
        }
    }

    @Override
    public boolean isLoggedIn(Long userId, String token) {
        final List<Auth> content = authRepository.findAuthByUserId(userId);
        if (!CollectionUtils.isEmpty(content)) {
            return !content.get(0).isBlocked() && content.get(0).getToken() != null && content.get(0).getToken().equals(token);
        } else {
            return false;
        }
    }

    @Override
    public void reload(Long userId) {
        final List<Auth> content = authRepository.findAuthByUserId(userId);
        if (!CollectionUtils.isEmpty(content)) {
            Auth auth = content.get(0);
            auth.setBlocked(false);
            authRepository.save(auth);
        }
    }

    @Override
    public Auth create(Long userId) {
        Auth auth = new Auth();
        auth.setUserId(userId);
        auth.setFailCounter(0);
        auth.setToken(UUID.randomUUID().toString());
        auth.setBlocked(false);
        authRepository.save(auth);
        return auth;
    }

    @Override
    public void delete(Long userId) {
        authRepository.findAuthByUserId(userId).stream().findFirst().ifPresent(authRepository::delete);
    }

    @Override
    public boolean updatePin(Long userId, Integer pin) {
        if (pin < 10000 && pin > 999) {
            authRepository.findAuthByUserId(userId).stream().findFirst().ifPresent(x -> {
                x.setPin(pin);
                authRepository.save(x);
            });
            return true;
        }
        return false;
    }

    @Override
    public boolean updateAuthType(Long id, String authType) {
        authRepository.findAuthByUserId(id).stream().findFirst().ifPresent(x -> {
            x.setAuthType(authType);
            authRepository.save(x);
        });
        return true;
    }
}
