package com.emulator.bank.service;

import com.emulator.bank.models.Auth;

public interface AuthService {

    String getToken(Long userId);

    boolean isPinOk(Long userId, Integer pin);

    boolean isBlocked(Long userId);

    void iterateError(Long userId);

    boolean isLoggedIn(Long userId, String token);

    void reload(Long userId);

    Auth create(Long userId);

    void delete(Long userId);

    boolean updatePin(Long userId, Integer pin);

    boolean updateAuthType(Long userId, String authType);
}
