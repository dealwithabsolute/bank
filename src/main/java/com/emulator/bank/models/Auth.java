package com.emulator.bank.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "auth", schema = "bank")
@DynamicUpdate
public class Auth {
    @Id
    @SequenceGenerator(name="pk_sequence",sequenceName="bank.auth_seq", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="pk_sequence")
    @Column(name = "id")
    private long id;
    @Column(name = "user_id")
    private long userId;
    @Column(name = "blocked", nullable = false)
    private boolean blocked = false;
    @Column(name = "pin")
    private Integer pin;
    @Column(name = "token")
    private String token = UUID.randomUUID().toString();
    @Column(name = "fail_counter")
    private Integer failCounter;
    @Column(name = "auth_type")
    private String authType;

    public void plusFailCounter() {
        this.failCounter = this.failCounter + 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Auth auth = (Auth) o;
        return blocked == auth.blocked && Objects.equals(id, auth.id) && Objects.equals(userId, auth.userId) && Objects.equals(pin, auth.pin) && Objects.equals(token, auth.token) && Objects.equals(failCounter, auth.failCounter) && Objects.equals(authType, auth.authType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, blocked, pin, token, failCounter, authType);
    }
}
