package com.emulator.bank.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import java.math.BigDecimal;
import java.util.Objects;


@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "user", schema = "bank")
@DynamicUpdate
public class User {
    @Id
    @SequenceGenerator(name="pk_sequence",sequenceName="bank.user_seq", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="pk_sequence")
    @Column(name = "id")
    private long id;
    @Column(name = "balance")
    private BigDecimal balance = BigDecimal.ZERO;

    public void plusBalance(BigDecimal balance) {
        this.balance = this.balance.add(balance);
    }

    public void minusBalance(BigDecimal balance) {
        this.balance = this.balance.subtract(balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(balance, user.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance);
    }
}
